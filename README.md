## IUNGO Liquibase manager 
#### v0.1
Lanciando lo script tramite shell è possibile eseguire diverse azioni, ognuna delle quale fornisce specifiche istruzioni.
```
sh iungo.sh
```
## Usage:
### Add IUNGO project
Selezionando l'opzione **Add IUNGO project** sarà possibile generare il file **.properties** ed un *launcher* **.sh** per effettuare le migrazioni tramite Liquibase. 

I vantaggi dell'usare lo script sono:
* I file creati verranno automaticamente organizzati in sottocartelle in base al nome del progetto.
* E' possibile creare i file tramite un unico comando.
* E' possibile lanciare la migrazione per un determinato progetto passandolo come argomento, senza dover andare a cercare manualmente lo script corrispettivo.

### Per generare i file tramite un unico comando:
```
sh iungo.sh -add nome-progetto nome-db "/path/to/sql/jar"
```
> La path del JAR SQL va' inserita tra virgolette se contiene spazi.

Il file **.iungo.properties** contiene le variabili che saranno riutilizzate in futuro per apportare delle modifiche a progetti già esistenti.

Il file **.liquibase.properties** viene generato con i dati necessari a lanciare l'update tramite Liquibase.

* * *

### Eseguire l'update sul changelog.xml tramite Liquibase

Dopo aver generato i file, per lanciare l'update di Liquibase sarà necessario solo il nome del progetto, ed eseguire il seguente comando:

```
sh iungo.sh -run nome-progetto
```

### Lanciare l'update sul dev_changelog.xml tramite Liquibase

```
sh iungo.sh -dev nome-progetto
```

### IUNGO < versione 10

Per versioni IUNGO precedenti alla 10 è necessario utilizzare MySQL5.
Per questo va specificata la versione MySQL5 quando si lancia il comando di creazione dei file:

```
sh iungo.sh -add nome-progetto nome-db "/path/to/sql/jar" mysql5

```
> In questo modo la porta utilizzata sarà la :3306