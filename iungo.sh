#!/usr/bin/env bash
version="v0.2"
echo ""

# Create folders structure
IUNGO_PROJECTS_FOLDER="./iungo-projects"
LAUNCHER_FOLDER="./launchers"
LAUNCHER_DEV_FOLDER="./launchers/dev"
if [ -d "$IUNGO_PROJECTS_FOLDER" ]; then
  continue;
else 
  mkdir $IUNGO_PROJECTS_FOLDER
fi
if [ -d "$LAUNCHER_FOLDER" ]; then
  continue;
else
  mkdir $LAUNCHER_FOLDER
fi
if [ -d "$IUNGO_PROJECTS_FOLDER" ]; then
  continue;
else 
  mkdir $LAUNCHER_DEV_FOLDER
fi

function box_info()
{
  local s=("$@") b w
  for l in "${s[@]}"; do
    ((w<${#l})) && { b="$l"; w="${#l}"; }
  done
  tput setaf 3
  echo " -${b//?/-}-
| ${b//?/ } |"
  for l in "${s[@]}"; do
    printf '| %s%*s%s |\n' "$(tput setaf 4)" "-$w" "$l" "$(tput setaf 3)"
  done
  echo "| ${b//?/ } |
 -${b//?/-}-"
  tput sgr 0
}

box_info 'IUNGO Liquibase Manager' $version

# source ./iungo-env.sh

prompt="Pick an option:"
options=("Generate new SQL migration" "Add IUNGO project" "List all IUNGO projects")
echo ""
addProject() {
  echo "Insert the project name: "
    #Create a properties file in which to store all the properties of a project.
    read ADD_PROJECT
    DIRECTORY="iungo-projects/$ADD_PROJECT"
    mkdir ./$DIRECTORY
    PROPERTIES_FILE="./$DIRECTORY/${ADD_PROJECT}.iungo.properties"
    touch $PROPERTIES_FILE
    echo "Insert the Database name: "
    read DB_NAME
    if [ "$DB_NAME" != "" ]; then
      echo DB_NAME=$SQL_JAR_PATH >> $PROPERTIES_FILE
    fi
    echo "Insert the path to the packaged SQL jar: "
    read SQL_JAR_PATH
    if [ "$SQL_JAR_PATH" != "" ]; then
      echo SQL_JAR_PATH=$SQL_JAR_PATH >> $PROPERTIES_FILE
    fi
    echo "Insert MySQL version (if the project is < iungo10 type: 5): "
    read MYSQL_VERSION
    if [ "$MYSQL_VERSION" != "" ]; then
      echo MYSQL_VERSION=$SQL_JAR_PATH >> $PROPERTIES_FILE
    fi
    #Create the liquibase properties file related to the project.
    echo "Process..."
    LIQUIBASE_PROPERTIES="${DIRECTORY}/${ADD_PROJECT}.liquibase.properties"
    touch $LIQUIBASE_PROPERTIES
    echo "logLevel=info" >> $LIQUIBASE_PROPERTIES
    echo "changeLogFile: META-INF/liquibase/changelog.xml" >> $LIQUIBASE_PROPERTIES
    echo "driver: com.mysql.jdbc.Driver" >> $LIQUIBASE_PROPERTIES
    echo "classpath: ${SQL_JAR_PATH}" >> $LIQUIBASE_PROPERTIES
    if ["$MYSQL_VERSION" != "5"]; then
      echo "url: jdbc:mysql://localhost:3307/${DB_NAME}?useSSL=false&createDatabaseIfNotExist=true" >> $LIQUIBASE_PROPERTIES
    else
      echo "The project use MySQL5, so it will use the port :3306 instead of :3307"
      echo "url: jdbc:mysql://localhost:3306/${DB_NAME}?useSSL=false&createDatabaseIfNotExist=true" >> $LIQUIBASE_PROPERTIES
    fi
    echo "username: root" >> $LIQUIBASE_PROPERTIES
    echo "password: IUNG0pwd" >> $LIQUIBASE_PROPERTIES
    echo "#liquibase.hub.mode=off" >> $LIQUIBASE_PROPERTIES
    echo "Done!"
    #Create the launcher for Liquibase
    echo "Create liquibase launcher..."
    LAUNCHER="./launchers/$ADD_PROJECT.liquibase.sh"
    touch $LAUNCHER
    echo "liquibase --logLevel=info --defaultsFile=${LIQUIBASE_PROPERTIES} --changeLogFile=META-INF/liquibase/changelog.xml update" >> $LAUNCHER
    DEV_LAUNCHER="./launchers/dev/$ADD_PROJECT-dev.liquibase.sh"
    echo "liquibase --logLevel=info --defaultsFile=${LIQUIBASE_PROPERTIES} --changeLogFile=META-INF/liquibase/dev_changelog.xml update" >> $DEV_LAUNCHER
}

quickAdd() {
  echo "Process..."
  DIRECTORY="iungo-projects/$1"
  mkdir ./$DIRECTORY
  PROPERTIES_FILE="./$DIRECTORY/${1}.iungo.properties"
  touch $PROPERTIES_FILE
  if [ "$2" != "" ]; then
      echo DB_NAME=$2 >> $PROPERTIES_FILE
  fi
  if [ "$3" != "" ]; then
      echo SQL_JAR_PATH=$3 >> $PROPERTIES_FILE
  fi
  LIQUIBASE_PROPERTIES="${DIRECTORY}/${1}.liquibase.properties"
  touch $LIQUIBASE_PROPERTIES
  echo "logLevel=info" >> $LIQUIBASE_PROPERTIES
  echo "changeLogFile: META-INF/liquibase/changelog.xml" >> $LIQUIBASE_PROPERTIES
  echo "driver: com.mysql.jdbc.Driver" >> $LIQUIBASE_PROPERTIES
  echo "classpath: ${3}" >> $LIQUIBASE_PROPERTIES
  if [ "$4" != "" ]; then
    echo "The project use MySQL5, so it will use the port :3306 insted of :3307"
    echo "url: jdbc:mysql://localhost:3306/${2}?useSSL=false&createDatabaseIfNotExist=true" >> $LIQUIBASE_PROPERTIES
  else
    echo "url: jdbc:mysql://localhost:3307/${2}?useSSL=false&createDatabaseIfNotExist=true" >> $LIQUIBASE_PROPERTIES
  fi
  echo "username: root" >> $LIQUIBASE_PROPERTIES
  echo "password: IUNG0pwd" >> $LIQUIBASE_PROPERTIES
  echo "#liquibase.hub.mode=off" >> $LIQUIBASE_PROPERTIES
  echo "Done!"
  #Create the launcher for Liquibase
  echo "Create liquibase launcher..."
  LAUNCHER="./launchers/${1}.liquibase.sh"
  touch $LAUNCHER
  echo "liquibase --logLevel=info --defaultsFile=${LIQUIBASE_PROPERTIES} --changeLogFile=META-INF/liquibase/changelog.xml update" >> $LAUNCHER
  DEV_LAUNCHER="./launchers/dev/${1}-dev.liquibase.sh"
  echo "liquibase --logLevel=info --defaultsFile=${LIQUIBASE_PROPERTIES} --changeLogFile=META-INF/liquibase/dev_changelog.xml update" >> $DEV_LAUNCHER
}

while getopts r:a:f:d: flag
do
    case "${flag}" in
        r|-run)
        project=${2}        
        source ./launchers/${project}.liquibase.sh
        exit 0
        ;;
        a|-add) 
        PROJECT_NAME=${2}
        _DB_NAME=${3}
        _SQL_JAR_PATH=${4}        
        echo "Project: $PROJECT_NAME \nDb name: $_DB_NAME \nSQL jar path: $_SQL_JAR_PATH"
        quickAdd $PROJECT_NAME $_DB_NAME $_SQL_JAR_PATH ${5}
        exit 0
        ;;
        f|-file) 
        fromFile=${2}
        exit 0
        ;;
        d|-dev) 
        project=${2}
        source ./launchers/dev/${project}-dev.liquibase.sh
        exit 0
        ;;
    esac
done

PS3="
$prompt "
select opt in "${options[@]}" "Quit"; do 
    case $opt in
    "Generate new SQL migration") 
    echo $opt
    ;;
    #Insert a new project with related properties into the iungo-projects directory.
    "Add IUNGO project")
    echo $opt
    addProject
    exit 0
    ;;
    "List all IUNGO projects") 
    echo $opt
    ;;
    "Quit")
    echo "Goodbye!"
    exit 0
    ;;
    *) echo "Invalid option. Try another one.";continue;;
    esac
done
